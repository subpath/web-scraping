"""Read pdf files, extract tabular data and save csv files."""
import os
from tabula import read_pdf
from tqdm import tqdm


def file_to_dataframe(file_path):
    """Read and save tables from pdf."""
    tables = read_pdf(file_path, multiple_tables=True)
    # extract and format potency table
    potency_table = tables[0]
    potency_table = potency_table.drop(potency_table.index[[0, 1]])
    potency_table = potency_table.drop(potency_table.columns[2], axis=1)
    potency_table.columns = ['cannabinoid', 'w/w']
    # extract and format terpenes table
    terpenes_table = tables[3]
    terpenes_table = terpenes_table.drop(terpenes_table.index[[0, 1]])
    terpenes_table = terpenes_table.drop(terpenes_table.columns[2], axis=1)
    terpenes_table.columns = ['terpenes', 'w/w']
    # save tables as csv
    potency_table.to_csv(file_path.replace('.pdf', '_potency.csv'))
    terpenes_table.to_csv(file_path.replace('.pdf', '_terpenes.csv'))

if __name__ == "__main__":
    pdf_files = [file for file in os.listdir('data') if file.endswith('pdf')]
    for pdf_file in tqdm(pdf_files, desc='Extracting'):
        try:
            file_to_dataframe('data/{}'.format(pdf_file))
        except IndexError:
            print('Problem with {}'.format(pdf_file))
