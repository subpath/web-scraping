# web-scraping

## How to run this project:

Python 3.6 is required

To run this project for the very first time:

0. ```pip install virtualenv```

1. ```virtualenv env -p python3```
2. ```source env/bin/activate```
3. ```pip install -r requirements.txt```
4. ```python data_scraping.py```
5. ```python data_extraction_from_pdf.py```

### This will save pdf files and then extract data into csv format and save it into ***data*** folder

To run this second+ time:

1. ```source env/bin/activate```
2. ```python data_scraping.py```
3. ```python data_extraction_from_pdf.py```



### Data I was able to download:

<https://drive.google.com/open?id=15uMBb3FzrvWTOPAZhYLQ_VZJXUnkhJlu>

